// valac --pkg json-glib-1.0 -X -w reverb_listings.vala
// sudo dnf install json-glib-devel
class ReverbListings : GLib.Object {
	public static void main(string[] args) {
		if (args.length > 1) {
			foreach (string product_type in args[1:]) {
				var uri = "https://api.reverb.com/api/listings?product_type=%s".printf(product_type);
				print_listings(uri);
			}
		} else {
			var uri = "https://api.reverb.com/api/listings";
			print_listings(uri);
		}
	}

	public static void print_listings(string uri) {
		try {
			var parser = new Json.Parser();
			var file = File.new_for_uri(uri);
			var stream = new DataInputStream(file.read());
			var builder = new StringBuilder ();
			string line;
			while ((line = stream.read_line()) != null) {
				builder.append(line);
			}
			parser.load_from_data(builder.str, -1);

			var root_object = parser.get_root().get_object();
			var listings = root_object.get_array_member("listings");

			foreach (var listing_obj in listings.get_elements()) {
				var listing = listing_obj.get_object();
				stdout.printf(
					"%s %s\n",
					listing.get_string_member("make"),
					listing.get_string_member("model"));
			}
		} catch (Error e) {
			error("%s", e.message);
		}
	}
}