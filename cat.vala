// valac -X -w cat.vala
class CatApp : GLib.Object {
    public static int main(string[] args) {
        if (args.length > 1) {
            foreach (string arg in args[1:]) {
                if (arg == "-") {
                    print_file(stdin);
                } else {
                    var file = FileStream.open(arg, "r");
                    if (file == null) {
                        stderr.printf("File '%s' doesn't exist\n", arg);
                        return 1;
                    }
                    print_file(file);
                }
            }
        } else {
            print_file(stdin);
        }
        return 0;
    }

    private static void print_file(FileStream file) {
        string? line;
        while ((line = file.read_line()) != null) {
            stdout.printf("%s\n", line);
        }
    }
}
