// valac --pkg gtk+-3.0 -X -w button_clicker.vala
// sudo dnf install gtk3-devel
delegate void ProcessCallback(string s);


void run_async_cmd(string[] args, ProcessCallback cb) {
	int standard_output;
	Pid child_pid;

	try {
		Process.spawn_async_with_pipes(
			".",
			args,
			{},
			SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
			null,
			out child_pid,
			null,
			out standard_output,
			null);
		ChildWatch.add(child_pid, (pid, status) => {
				Process.close_pid(pid);
				if (status == 0) {
					try {
						var channel = new IOChannel.unix_new(standard_output);
						string data;
						channel.read_to_end(out data, null);
						cb(data);

					} catch (Error e) {
						stderr.printf("%s\n", e.message);
					}
				}
			});
	} catch (Error e) {
		stderr.printf("%s\n", e.message);
	}

}

class ButtonClicker: GLib.Object {
    public static int main (string[] args) {
        Gtk.init (ref args);

        var window = new Gtk.Window();
        window.title = "Button Clicker";
        window.border_width = 10;
        window.window_position = Gtk.WindowPosition.CENTER;
        window.set_default_size(350, 70);
        window.destroy.connect(Gtk.main_quit);

        var grid = new Gtk.Grid();
        window.add(grid);

        int clicked_times = 0;

        var button = new Gtk.Button.with_label("Clicked %d times.".printf(clicked_times));
        button.clicked.connect (() => {
                button.label = "Clicked %d times.".printf(++clicked_times);
        });

        grid.add(button);

        var date_button = new Gtk.Button.with_label("Get date");
        date_button.clicked.connect (() => {
				run_async_cmd({"date"}, (date) => {
						date_button.label = "Date: %s".printf(date.strip());
					});
        });

        grid.add(date_button);

        window.show_all();

        Gtk.main();
        return 0;
    }
}