Vala Stuff
==========

Some programs in vala to practice doing some things you can do in vala.

You can install vala in fedora with `sudo dnf install vala`. See each
program for required dependencies and a compilation command.
